## The Country Neighbors Tour

** GET Calculate visits **  
/v1/countries/BG?budgetPerCountry=10&totalBudget=120&currency=EUR

The API calculate how many exact times can Angel go through all neighbor countries
within his total budget.

** Query Params **
* budgetPerCountry  
* totalBudget  
* currency 